<?php
namespace PauloColiver\Calculator;

use \Exception;

/**
 * Class Calculator
 *
 * @package PauloColiver\Calculator
 */
class Calculator {

    /**
     * @var int
     */
    private $blocks_open = 0;

    /**
     * Calculator constructor.
     *
     * @param $text
     * @throws Exception
     */
    private function __construct()
    {
    }

    /**
     * @param string $text
     * @return mixed
     * @throws Exception
     */
    public static function calculate ($text) {

        $_this = (new static());

        $text = str_replace(' ', '', trim($text));

        if (preg_match_all("/[^(\d|\)|\)|\+|\-|\*|\/|\.|\^|%)]/", $text)) {
            throw new Exception('Invalid');
        }

        $dec1 = '((\d+(\.\d+))|\d+)';
        $dec2 = '(M(\d+)(\.\d+)|M\d+)';
        $dec3 = "($dec1|$dec2)";
        $text = preg_replace("/([\D]|^)(-)$dec1/", "\${1}M\${3}", $text);

        preg_match_all("/(\()|(\))|$dec3|[\.\+\-\*\/\^|%]/", $text,$output_array);
        $blocks = $_this->separateByParentheses($output_array[0]);
        if ($_this->blocks_open > 0)
            throw new \Exception('Invalid expression');

        return $_this->executeExpressions($blocks);
    }

    /**
     * @param $exp
     * @param $base
     * @return float|int
     */
    private function pow($exp, $base) {
        $original_base = $base;
        $original_exp  = $exp;
        $res_negative  = false;

        $result = $base;
        $base = $base<0 ? ($base*-1) : $base;

        if ($exp < 0) {
            $result = $base = 1 / $base;
            $exp *= -1;
            if ($original_base < 0) {
                $res_negative = true;
            }
        }

        $i = 1;
        while ($i < (int)$exp) {
            $result *= $base;
            $i++;
        }
        return $res_negative ? -$result : $result;
    }

    /**
     * @param $operator
     * @param $a
     * @param $b
     * @return float|int
     */
    private function executeOperator($operator, $a, $b) {
        if ($operator === '+') {
            return $a + $b;

        } else if ($operator === '-') {
            return $a - $b;

        } else if ($operator === '%') {
            return $a % $b;

        } else if ($operator === '*') {
            return $a * $b;

        } else if($operator === '/') {
            if($b === 0) {
                throw new \InvalidArgumentException('Division by zero occured');
            }
            return $a / $b;

        } else if($operator === '^') {
            return $this->pow($b, $a);
        }

        throw new \InvalidArgumentException('Unknown operator provided');
    }

    /**
     * @param $text
     * @param int $i
     * @param int $k
     * @return array
     * @throws Exception
     */
    private function separateByParentheses ($text, &$i=0, $k=0) {

        $res = [];
        $max = count($text);

        do {
            $piece = $text[$i];
            $i++;

            if ($piece == '(') {
                $this->blocks_open += 1;
                $res[$i] = $this->separateByParentheses($text, $i, $k+1);
            } elseif ($piece == ')') {
                if ($this->blocks_open == 0)
                    throw new \Exception('Invalid expression');

                $this->blocks_open -= 1;
                return $res;
            } else {
                $res[$i] = preg_replace("/M/", "-$2", $piece);
            }
        } while ($i<$max);

        return $res;
    }

    /**
     * @param array $expressions
     * @return float
     * @throws Exception
     */
    private function executeExpressions (array $expressions) {

        $text = '';
        foreach ($expressions as $expression) {

            if (is_array($expression)) {
                $expression = $this->executeExpressions($expression);
            }

            $text .= $expression;
        }

        return $this->calculateExpression($text);
    }

    /**
     * @param string $text
     * @return string
     */
    private function calculateExpression ($text) {

        $dec1 = '(\d+\.\d+|\d+)';
        $dec2 = '(-\d+\.\d+|-\d+)';
        $dec3 = "($dec1|$dec2)";

        $regex_operators = [
            "/($dec3\^$dec3)/",
            "/($dec3\*$dec3)|($dec3\/$dec3)/",
            "/($dec3%$dec3)/",
            "/($dec3\+$dec3)|($dec3\-$dec3)/",
        ];
        $i = 0;

        do {

            if (empty($regex_operators[$i]))
                break;

            preg_match_all($regex_operators[$i], $text, $itens);

            if (!empty($itens[0])) {
                $new_values = [];
                foreach ($itens[0] as $value) {
                    preg_match_all("/$dec3|./", $value, $item);
                    $new_values[$value] = $this->executeOperator($item[0][1], (float) $item[0][0], (float) $item[0][2]);

                }
                $text = strtr($text, $new_values);
            } else {
                $i++;
            }

        } while (true);

        return $text;
    }

}